#!/bin/bash

FILE=commands.txt

while true; do

command="`sed -n -e $(shuf -i1-$(wc -l $FILE | cut -d' ' -f 1) -n 1)p $FILE`"
clear

echo "$command" | bash | cowsay

sleep 4
done 
